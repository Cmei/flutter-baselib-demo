import 'dart:convert';

import 'package:flutter_baselib/flutter_baselib.dart';

///@date:  2021/3/11 14:06
///@author:  lixu
///@description: http请求使用的工具类
class HttpUtils {
  ///TODO 可根据后端定义的算法修改即可
  ///获取请求头sign
  ///[params] 请求参数map
  ///[key] 加密key（服务端提供）
  static Future<String> getSignEncode(Map<String, dynamic> paramsMap, String key) async {
    String requestJson = json.encode(paramsMap);
    LogUtils.e('HttpUtils', requestJson);
    String finalSign = await EncodeUtils.generateMd5(requestJson + key);
    return finalSign;
  }
}
