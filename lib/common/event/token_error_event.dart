///@date:  2021/5/18 14:24
///@author:  lixu
///@description: token失效事件
class TokenErrorEvent {
  ///错误码区分登录信息失效的类型
  String code;
  String? msg;

  TokenErrorEvent(this.code, this.msg);

  @override
  String toString() {
    return '[$code] $msg';
  }
}
