import 'package:flutter_baselib/flutter_baselib.dart';

///@date:  2021/5/20 09:27
///@author:  lixu
///@description: 管理用户登录状态和用户信息
///View与该ViewModel绑定后：用户信息或登录状态改变时，会通知view刷新
class UserInfoViewModel extends BaseCommonViewModel {
  @override
  String getTag() {
    return 'UserInfoViewModel';
  }
}
