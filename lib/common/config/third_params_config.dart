import 'package:baselib_demo/common/util/env/dev_env_utils.dart';

///@date:  2021/5/12 11:35
///@author:  lixu
///@description: 保存三方参数
class ThirdParamsConfig {
  //TODO
  ///微信
  static const String _releaseWechatAppId = 'wx8f62b95d99b0c206';
  static const String _testWechatAppId = 'wx8f62b95d99b0c206';

  ///QQ
  static const qqAppId = '1111682679';

  ///手机号一键登录（蓝创闪验）
  static const shanYanAppId = 'OuDzUjpF';

  ///bugly appId
  static const buglyAppId = 'fc8411f706';

  ///获取微信appAppId
  ///正式服与测试服用不同的参数
  static Future<String> getWechatAppId() async {
    bool isRelease = await DevEnvUtils.getIsReleaseServerEnv();
    return isRelease ? _releaseWechatAppId : _testWechatAppId;
  }
}
