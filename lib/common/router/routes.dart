import 'package:fluro/fluro.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_baselib/flutter_baselib.dart';
import 'package:baselib_demo/common/router/router_handler.dart';

///@date:  2021/4/20 12:44
///@author:  lixu
///@description:路由注册管理
class Routes {
  static const String _tag = 'Routes';

  static FluroRouter router = FluroRouter();

  static String root = '/';
  static String debugPage = '/debugPage';
  static String loginPage = '/loginPage';
  static String homePage = '/homePage';

  ///应用启动时初始化路由
  static void initRouters() {
    router.notFoundHandler = Handler(handlerFunc: (context, Map<String, List<String>> params) {
      LogUtils.e(_tag, "ROUTE NOT FOUND");
      return;
    });

    ///启动页
    router.define(root, handler: RouterHandler.rootHandler, transitionType: TransitionType.fadeIn);

    ///调试页
    router.define(debugPage, handler: RouterHandler.debugHandler);

    ///登录
    router.define(loginPage, handler: RouterHandler.loginHandler);

    ///主界面
    router.define(homePage, handler: RouterHandler.homeHandler);
  }

  ///对参数进行encode，解决参数中有特殊字符，影响fluro路由匹配
  ///所有页面跳转通过该方法，便于统一管理
  static Future navigateTo(BuildContext context, String path,
      {Map<String, dynamic>? params,
      bool replace = false,
      bool clearStack = false,
      TransitionType transition = TransitionType.fadeIn,
      Duration transitionDuration = const Duration(milliseconds: 250),
      RouteTransitionsBuilder? transitionBuilder}) {
    if (params != null) {
      String query = "";
      int index = 0;
      for (var key in params.keys) {
        var value = Uri.encodeComponent(params[key].toString());
        if (index == 0) {
          query = "?";
        } else {
          query = query + "\&";
        }
        query += "$key=$value";
        index++;
      }
      path = path + query;
    }

    return router.navigateTo(
      context,
      path,
      replace: replace,
      clearStack: clearStack,
      transition: transition,
      transitionDuration: transitionDuration,
      transitionBuilder: transitionBuilder,
    );
  }
}
