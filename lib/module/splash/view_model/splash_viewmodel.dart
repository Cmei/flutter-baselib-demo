import 'package:flutter/material.dart';
import 'package:flutter_baselib/flutter_baselib.dart';
import 'package:baselib_demo/common/router/navigator_utils.dart';

///@date:  2021/4/20 16:17
///@author:  lixu
///@description: 启动页viewMode
class SplashViewModel extends BaseCommonViewModel {
  @override
  String getTag() {
    return 'SplashViewModel';
  }

  ///进入下一页
  Future? goNextPage(BuildContext context) {
    ///TODO 根据登录状态进入不同页面
    NavigatorUtils.toLogin(context);
  }
}
