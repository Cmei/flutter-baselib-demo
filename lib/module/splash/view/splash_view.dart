import 'package:flutter/material.dart';
import 'package:flutter_baselib/flutter_baselib.dart';
import 'package:baselib_demo/common/util/assets_utils.dart';
import 'package:baselib_demo/module/splash/view_model/splash_viewmodel.dart';

///@date:  2021/4/20 16:03
///@author:  lixu
///@description: 启动页
class SplashView extends StatefulWidget {
  @override
  _SplashViewState createState() => _SplashViewState();
}

class _SplashViewState extends State<SplashView> with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _animation;
  late BuildContext _context;

  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: Duration(seconds: 1),
    );

    _animation = Tween(
      begin: 0.0,
      end: 1.0,
    ).animate(_controller);

    _controller.forward();

    _animation.addStatusListener((AnimationStatus status) {
      if (status == AnimationStatus.completed) {
        _context.read<SplashViewModel>().goNextPage(_context);
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ChangeNotifierProvider(
        create: (_) {
          return SplashViewModel();
        },
        builder: (context, child) {
          _context = context;

          return FadeTransition(
            opacity: _animation,
            child: Container(
                child: Center(
              child: Image.asset(
                AssetsUtils.loadAssetsImg('logo'),
                width: 100,
                height: 100,
              ),
            )),
          );
        },
      ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}
