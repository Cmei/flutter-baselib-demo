import 'package:flutter/material.dart';
import 'package:flutter_baselib/flutter_baselib.dart';
import 'package:baselib_demo/common/util/ui_utils.dart';
import 'package:baselib_demo/module/home/home_tabview/choiceness_tabview.dart';
import 'package:baselib_demo/module/home/home_tabview/newgame_tabview.dart';
import 'package:baselib_demo/module/home/home_tabview/rank_tabview.dart';
import 'package:baselib_demo/module/home/view_model/home_view_model.dart';
import 'package:baselib_demo/module/home/view_model/hometab/choiceness_view_model.dart';
import 'package:baselib_demo/module/home/view_model/hometab/newgame_view_model.dart';
import 'package:baselib_demo/module/home/view_model/hometab/rank_view_model.dart';

///@date:  2021/5/18 11:25
///@author:  lixu
///@description: app主界面->底部tab1：首页tab
class HomeViewTab extends StatefulWidget {
  @override
  _HomeViewTabState createState() => _HomeViewTabState();
}

class _HomeViewTabState extends State<HomeViewTab> {
  static const _tabBarTextStyle = TextStyle(fontSize: 15);

  final _tabs = [
    Tab(child: Text('精选')),
    Tab(child: Text('新游')),
    Tab(child: Text('排行')),
  ];

  final _tabViews = [
    BaseView<ChoicenessViewModel>(
      child: ChoicenessTabView(),
    ),
    BaseView<NewGameViewModel>(
      child: NewGameTabView(),
    ),
    BaseView<RankViewModel>(
      child: RankTabView(),
    ),
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) {
          return HomeViewModel();
        }),
        ChangeNotifierProvider(create: (_) {
          return ChoicenessViewModel();
        }),
        ChangeNotifierProvider(create: (_) {
          return NewGameViewModel();
        }),
        ChangeNotifierProvider(create: (_) {
          return RankViewModel();
        }),
      ],
      builder: (context, child) {
        return Theme(
          data: Theme.of(context).copyWith(splashColor: Colors.transparent, highlightColor: Colors.transparent),
          child: DefaultTabController(
            length: _tabs.length,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: 40,
                  margin: EdgeInsets.only(top: UIUtils.statusBarHeight, left: 10),
                  child: TabBar(
                    isScrollable: true,
                    indicatorColor: Colors.pinkAccent,
                    indicatorPadding: EdgeInsets.symmetric(horizontal: 10),
                    indicatorSize: TabBarIndicatorSize.label,
                    indicatorWeight: 3,
                    unselectedLabelColor: Colors.grey,
                    unselectedLabelStyle: const TextStyle(
                      fontSize: 15,
                    ),
                    labelColor: Colors.pinkAccent,
                    labelStyle: const TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                    ),
                    labelPadding: EdgeInsets.symmetric(horizontal: 10),
                    tabs: _tabs,
                  ),
                ),
                Expanded(
                  child: TabBarView(
                    children: _tabViews,
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
