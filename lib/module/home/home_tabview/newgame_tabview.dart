import 'package:flutter/material.dart';
import 'package:flutter_baselib/flutter_baselib.dart';

///@date:  2021/5/18 11:31
///@author:  lixu
///@description: app主界面，底部首页tab包含的顶部：新游tab
class NewGameTabView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    LogUtils.i("NewGameTabView", "build");
    return Container(
      child: Center(
        child: Text('新游'),
      ),
    );
  }
}
