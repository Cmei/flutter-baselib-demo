import 'package:flutter/material.dart';
import 'package:flutter_baselib/flutter_baselib.dart';

///@date:  2021/4/20 16:14
///@author:  lixu
///@description: 主界面MainPage使用ViewModel
class MainViewModel extends BaseCommonViewModel {
  ///上次点击时间
  DateTime? _lastPressedAt;

  @override
  String getTag() {
    return 'MainViewModel';
  }

  ///首页处理返回按钮逻辑
  WillPopCallback onWillPop() {
    return () async {
      if (_lastPressedAt == null || DateTime.now().difference(_lastPressedAt!) > Duration(seconds: 3)) {
        _lastPressedAt = DateTime.now();
        ToastUtils.show("再次点击退出");
        return false;
      } else {
        ///这里退出 app
        return true;
      }
    };
  }
}
