import 'package:flutter_baselib/flutter_baselib.dart';
import 'package:baselib_demo/common/net/http_urls.dart';
import 'package:baselib_demo/module/login/bean/login_result_bean.dart';

///@date:  2021/5/20 09:25
///@author:  lixu
///@description: 首页福利tab，使用的ViewModel
class WelfareViewModel extends BaseViewModel<LoginResultBean> {
  @override
  bool configIsRequestListData() {
    return false;
  }

  @override
  String getTag() {
    return 'WelfareViewModel';
  }

  @override
  Map<String, dynamic> getRequestParams() {
    return {
      'account': '15015001500',
      'pass': '123qwe',
      'appType': 'PATIENT',
      'device': 'ANDROID',
      'push': '13065ffa4e22e63efd2',
    };
  }

  @override
  String getUrl() {
    return HttpUrls.loginUrl;
  }
}
