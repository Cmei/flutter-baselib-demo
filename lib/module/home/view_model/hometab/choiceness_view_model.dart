import 'package:flutter_baselib/flutter_baselib.dart';

///@date:  2021/5/20 10:50
///@author:  lixu
///@description: app主界面，tab1顶部：精选tab使用的ViewModel
class ChoicenessViewModel extends BaseCommonViewModel {
  @override
  String getTag() {
    return 'ChoicenessViewModel';
  }
}
