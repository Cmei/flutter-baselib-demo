import 'package:flutter/cupertino.dart';
import 'package:flutter_baselib/flutter_baselib.dart';
import 'package:baselib_demo/common/event/token_error_event.dart';

///@date:  2021/5/18 14:17
///@author:  lixu
///@description: 退出登录的viewModel，全局有效
class LogoutViewModel extends BaseCommonViewModel {
  @override
  String getTag() {
    return 'LogoutViewModel';
  }

  ///退出登录时调用
  Future<void> onLogout(BuildContext context) async {
    LogUtils.d(getTag(), 'onLogout');

    ///取消其它http请求
    api.cancelAll();

    ///清除登录信息

    ///跳转到指定页面
    ///TODO
  }

  ///处理登录信息失效逻辑
  void onTokenErrorLogic(BuildContext context, TokenErrorEvent event) {
    ///TODO 显示弹窗提示用户,跳转指定页面
    ToastUtils.show('登录信息失效:${event.toString()}');

    ///TODO
    ///
  }
}
