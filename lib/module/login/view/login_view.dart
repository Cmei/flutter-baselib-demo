import 'package:flutter/material.dart';
import 'package:flutter_baselib/flutter_baselib.dart';
import 'package:baselib_demo/common/util/ui_utils.dart';
import 'package:baselib_demo/module/login/view_model/login_view_model.dart';

import '../../../common/router/navigator_utils.dart';

///@date:  2021/3/1 12:00
///@author:  lixu
///@description: 登录页
class LoginView extends StatefulWidget {
  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('登录页'),
      ),
      body: ChangeNotifierProvider(
        create: (_) {
          return LoginViewModel();
        },
        child: Consumer<LoginViewModel>(
          builder: (context, viewModel, child) {
            return Container(
              alignment: Alignment.center,
              child: Column(
                children: [
                  UIUtils.getButton('账号登录接口（测试）', () {
                    viewModel.onLogin(context);
                  }),
                  UIUtils.getButton('进入debug页面', () {
                    NavigatorUtils.toDebugPage(context);
                  }),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
