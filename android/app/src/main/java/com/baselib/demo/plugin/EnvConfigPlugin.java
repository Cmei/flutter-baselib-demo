package com.baselib.demo.plugin;

import android.content.Context;

import androidx.annotation.NonNull;

import com.baselib.demo.BuildConfig;

import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;

/**
 * 获取项目环境配置的插件
 * 本地调试运行或本地打包时：获取android工程下的config.gradle配置文件中的参数
 * jenkins打包时：获取jenkins传递的配置参数
 */
public class EnvConfigPlugin implements MethodCallHandler {
    private static final String TAG = "SysEnvConfigPlugin";
    /**
     * 获取服务器环境
     */
    private final static String _isReleaseServerMethod = "isReleaseServer";

    /**
     * 获取控制台是否打印日志状态
     */
    private final static String _isConsoleLogMethod = "isConsoleLog";

    /**
     * 代码是否混淆
     */
    private final static String _isProguardMethod = "isProguard";

    private MethodChannel channel;

    public static void registerWith(BinaryMessenger messenger, Context context) {
        MethodChannel channel = new MethodChannel(messenger, "com.qw.flutter.plugins/env");
        channel.setMethodCallHandler(new EnvConfigPlugin());
    }


    @Override
    public void onMethodCall(@NonNull MethodCall call, @NonNull final Result result) {
        if (_isReleaseServerMethod.equals(call.method)) {
            //获取服务器环境
            result.success(BuildConfig.IS_RELEASE_SERVER);
        } else if (_isConsoleLogMethod.equals(call.method)) {
            //获取控制台是否打印日志状态
            result.success(BuildConfig.IS_CONSOLE_LOG);
        } else if (_isProguardMethod.equals(call.method)) {
            //代码是否混淆
            result.success(BuildConfig.IS_PROGUARD);
        } else {
            result.notImplemented();
        }
    }

}
